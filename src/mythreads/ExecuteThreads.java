package mythreads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThreads {

    public static void main(String[] args) {

        ExecutorService paintingCars = Executors.newFixedThreadPool(3);

        MyRunnable modelOne = new MyRunnable("Blue", "Corolla", 5);
        MyRunnable modelTwo = new MyRunnable("Red", "Camry", 10);
        MyRunnable modelThree = new MyRunnable("Green", "Rav4", 15);

        paintingCars.execute(modelOne);
        paintingCars.execute(modelTwo);
        paintingCars.execute(modelThree);

        paintingCars.shutdown();
    }
}
