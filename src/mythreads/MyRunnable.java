package mythreads;

public class MyRunnable implements Runnable {

    private String color;
    private String model;
    private int time;

    public MyRunnable(String color, String model, int time) {

        this.model = model;
        this.time = time;
        this.color = color;
    }

    public void run() {
        System.out.println("\n\n*** Here are the cars being painted today ***\nModel: " + model + "\nColor: " + color + "\nEstimated time to paint: " + time + " Second(s).");
        for (int i = 1; i < time; i++) {
            if (i != time) {
                System.out.print("\n" + model + " not done painting yet. Time remaining " + (time - i) + " second(s)");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + model + " is done painting. Enjoy your car!\n\n");
    }
}
